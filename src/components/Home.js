import React, { useState } from 'react'
import { usePDF } from 'react-to-pdf'

function Home() {
  const { toPDF, targetRef } = usePDF({filename: 'Document.pdf'});
  const[firstStep,setFirstStep]=useState(false)
  const[estimate,setEstimate]=useState({
    estimation:'',
    nbrOffres:''
  })

  const handleForm=(e)=>{
    setEstimate({
      ...estimate,
      [e.target.name]:e.target.value
    })
  }

  const handleCalculate=(e)=>{
    e.preventDefault()
    if(estimate.nbrOffres>0 && estimate.estimation>0){
      setFirstStep(true)
    }else{
      return setMsg('Tous les champes sont obligatoires!')
    }
  }
  

  let nbrOffres=Number(estimate.nbrOffres)
  let inputOffresArray = Array.from({ length: nbrOffres }, (_,index) => (index+1));

  const[prixOffs,setPrixOffs]=useState([])

  const handleOffres = (e, index) => {
    const updatedPrixOffs =[...prixOffs];
    updatedPrixOffs[index] = e.target.value;
    setPrixOffs(updatedPrixOffs);
  };
  

  // calculerReferentiel start
  let P=0
  // let offre_proche=0
  let entries_offres=prixOffs

  const[msg,setMsg]=useState('')
  const[result,setResult]=useState({
    prix_referentiel:'',
    // offre_plus_proche:''
  })


  const calculerReferentiel=()=>{
    try {
      const E=parseFloat(estimate.estimation)
      const NOF=parseFloat(estimate.nbrOffres)
      const offres=entries_offres.map(item=>parseFloat(item))
      const SM=offres.reduce((acc,curr)=>curr+acc ,0)
      P=((E*NOF+SM)/NOF)*0.5
      // offre_proche=Math.min(...offres.map(x=> Math.abs(x-P)))

     //------------------------testing---------------------------------
      // offre_proche = offres.reduce((prev, curr) => {
      // const prevDiff = Math.abs(prev - P);
      // const currDiff = Math.abs(curr - P);
      // return currDiff < prevDiff ? curr : prev;
      // });

      // const obj={
      //   'pr':P,
      //   'ofpp':offre_proche
      // }
      // return  obj


      setMsg('')
      setResult({
        ...result,
        prix_referentiel:P.toFixed(2),
        // offre_plus_proche:offre_proche.toFixed(2)
      })
    } catch (error) {
      console.log(error) //this result 
    }
  }

  // calculerReferentiel end

  const[resultats,setResultats]=useState(false)
  const resArea=()=>{
    if(result.prix_referentiel!==''){
      let offGraterThenP=entries_offres.filter((item)=>{
        return Number(item)>Number(result.prix_referentiel)
      })
      let offLowerThenP=entries_offres.filter((item)=>{
        return Number(item)<Number(result.prix_referentiel)
      })

      const obj={
        max:offGraterThenP,
        min:offLowerThenP
      }
      return obj
    }
  }

  const Calculer=(e)=>{
    e.preventDefault()
    calculerReferentiel()
    setResultats(true)
  }
  

  
  const style_pr={color:"green", fontSize:20}
  const msg_style={color:"red",fontSize:15}
  const formCPR=(
    <form className='bg-white p-3 rounded shadow-sm'>
      <div className='mb-3'>
        <h2>Calculer prix Référentiel</h2>
      </div>
      <div className="mb-3">
        <label htmlFor="exampleInputEmail1" className="form-label">Estimation initial (E):</label>
        <input type="number" name='estimation' onChange={handleForm} className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"/>
      </div>
      <div className="mb-3">
        <label htmlFor="exampleInputPassword1" className="form-label">Nombre des offres financières (NOF):</label>
        <input type="number" name='nbrOffres' onChange={handleForm} className="form-control" id="exampleInputPassword1"/>
      </div>
      <button type="submit" onClick={handleCalculate} className="btn btn-primary me-1">Calculer</button>
      {/* show result of the first instruction from the calculerReferentiel function */}
      {result.prix_referentiel==='' ?(
      // && result.offre_plus_proche===''
        <span></span>
      ):(
        <>
          <hr/>
          <p><span style={style_pr}>Prix referentiel: </span>{result.prix_referentiel}</p>
          {/* <p><span style={style_pr}>Offres plus proche: </span>{result.offre_plus_proche}</p> */}
        </>
      )}

      {/* check form inputs validation */}

      {msg===''?(
        <span></span>
      ):(
        <p style={msg_style}>{msg}</p>
      )}
    </form>
  )

  return (
    <div className='container-fluid'>
        <div className='row border'>
            <div className='col-12 col-md-5 col-lg-5 Left-side'>
                <div className='col-12 col-md-6 col-lg-6 Form-area'>
                  {firstStep===false?(
                      formCPR
                  ):(
                    resultats===false?(
                        <>
                        {inputOffresArray.map((item,index)=>{
                          return <div key={index}>
                                  <div className="mb-3">
                                    <label htmlFor={`offre-${index}`} className="form-label">Offre: {index+1}</label>
                                    <input 
                                    type="number" 
                                    onChange={(e)=>handleOffres(e,index)} 
                                    value={prixOffs[index]||''}
                                    name='offres_prix' 
                                    className="form-control" 
                                    id={`offres-${index}`} 
                                    aria-describedby="emailHelp"/>
                                  </div>
                                  </div>
                        })}
                        <button onClick={Calculer} className='btn btn-primary'>Calculer</button>
                        </>
                    ):(
                      // <span>{`${resArea().min}<${result.prix_referentiel}<${resArea().max}`}</span>
                      <div>
                        <table class="table table-striped" ref={targetRef}>
                          <thead>
                            <tr>
                              <th scope="col">ID</th>
                              <th scope="col">Offres Max</th>
                              <th scope="col">Prix referentiel</th>
                              <th scope="col">Offres Min</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <th scope="row">1</th>
                              {/* max offres */}
                              <td>{resArea().max.length>1?(
                                 resArea().max.map((td,i)=>{
                                  return <tr key={i}>
                                          <td>{td} Dh</td>
                                         </tr>
                                 })
                              ):(
                                <td>{resArea().max} Dh</td>
                              )}</td>
                              {/* prix referentiel */}
                              <td>{result.prix_referentiel} Dh</td>
                              {/* min offres */}
                              <td>{resArea().min.length>1?(
                                 resArea().min.map((td,i)=>{
                                  return <tr key={i}>
                                          <td>{td} Dh</td>
                                         </tr>
                                 })
                              ):(
                                <td>{resArea().min} Dh</td>
                              )}</td>
                            </tr>
                          </tbody>
                        </table>
                        <button onClick={() => toPDF()} className='btn btn-primary'>Download PDF</button>
                      </div>
                    )

                    
                  )}
                </div>
            </div>
            <div className='col-12 col-md-5 col-lg-5 Rigth-side'>
                
            </div>
            
        </div>
    </div>
  )
}






export default Home 


